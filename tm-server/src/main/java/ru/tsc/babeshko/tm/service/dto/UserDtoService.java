package ru.tsc.babeshko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.api.service.IPropertyService;
import ru.tsc.babeshko.tm.api.service.dto.IProjectDtoService;
import ru.tsc.babeshko.tm.api.service.dto.ITaskDtoService;
import ru.tsc.babeshko.tm.api.service.dto.IUserDtoService;
import ru.tsc.babeshko.tm.dto.model.UserDto;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.exception.entity.UserNotFoundException;
import ru.tsc.babeshko.tm.exception.field.*;
import ru.tsc.babeshko.tm.repository.dto.UserDtoRepository;
import ru.tsc.babeshko.tm.util.HashUtil;

import java.util.Optional;

@Service
public class UserDtoService extends AbstractDtoService<UserDto> implements IUserDtoService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private UserDtoRepository repository;

    @NotNull
    @Override
    protected UserDtoRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDto create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setEmail(email);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        if (role == null) user.setRole(Role.USUAL);
        else user.setRole(role);
        repository.save(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDto findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return repository.findFirstByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDto findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmptyEmailException();
        return repository.findFirstByEmail(email);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        taskService.removeAllByUserId(user.getId());
        projectService.removeAllByUserId(user.getId());
        remove(user);
    }

    @NotNull
    @Override
    public UserDto setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDto user = findOneById(id);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        update(user);
        return user;
    }

    @NotNull
    @Override
    public UserDto updateUser(
            @NotNull final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final UserDto user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final UserDto user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLock(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final UserDto user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLock(false);
        update(user);
    }

}
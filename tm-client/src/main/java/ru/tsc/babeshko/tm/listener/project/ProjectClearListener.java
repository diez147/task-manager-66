package ru.tsc.babeshko.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.dto.request.ProjectClearRequest;
import ru.tsc.babeshko.tm.event.ConsoleEvent;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[PROJECTS CLEAR]");
        getProjectEndpoint().clearProject(new ProjectClearRequest(getToken()));
    }

}
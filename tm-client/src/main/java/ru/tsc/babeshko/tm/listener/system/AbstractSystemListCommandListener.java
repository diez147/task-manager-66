package ru.tsc.babeshko.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@Component
public abstract class AbstractSystemListCommandListener extends AbstractSystemListener {

    @Nullable
    @Autowired
    private AbstractListener[] commands;

    public Collection<AbstractListener> getCommands() {
        return Arrays.asList(commands);
    }

    protected Collection<AbstractListener> getArguments() {
        return Arrays
                .asList(commands)
                .stream()
                .filter(arg -> arg.getArgument() != null && !arg.getArgument().isEmpty())
                .collect(Collectors.toList());
    }

}

